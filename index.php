<?php
try
{
    $bdd = new PDO('mysql:host=localhost;dbname=todolist;charset=utf8', 'root', 'root');
}
catch(Exception $e)
{
    die('Erreur : '.$e->getMessage());
}
$reponse = $bdd->query('SELECT * FROM listitems WHERE id_item > 0 ORDER BY id_item DESC LIMIT 0, 30');
if ($_POST){
    $name_item=$_POST["item"];
    if (!empty($name_item)){
        $requete = $bdd->prepare("INSERT INTO listitems (name_item) VALUES (?)");
        $requete -> execute(array($name_item));
    }

}



?><!doctype html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="./style.css" >
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                <h1 id="titre">TO DO LIST</h1>
            </div>
        </div>
    </div>
    <div class="container" id="inputItems">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                <form method="POST" action="traitement.php">
                    <input class="champ" type="text" name="item" placeholder="Saisir des taches à éffectuer">
                    <button type="submit" class="btnchamp">Enregistrer</button>
                </form>
            </div>
        </div>
    </div>
    <?php
    while ($donnees = $reponse->fetch()){
    ?><div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 ">
                <table id="listitemlive">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <!--<td><a><img class="image"src="img/valid.png" height="15" width="15"></a></td>
                            <td><a><img class="image"src="img/not-valid.png" height="15" width="15"></a></td>-->
                            <td><h3><?php  echo $donnees['name_item']?></h3></td>
                            <td><a class="sup" href="supp.php?delete=<?php echo $donnees['id_item'] ?>"><img src="img/not.png" width="15" height="15"></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php }
    $reponse->closeCursor();
    ?><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</body>
</html>