<?php
try
    {
        $bdd = new PDO('mysql:host=localhost;dbname=todolist;charset=utf8', 'root', 'root');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
    if ($_POST){
        $name_item=$_POST["item"];
        if (!empty($name_item)){
            $requete = $bdd->prepare("INSERT INTO listitems (name_item) VALUES (?)");
            $requete -> execute(array($name_item));
            header('Location: index.php');
        }
    } ?>
